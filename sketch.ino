////////////////////////////////////////////////////////////////////////////////
//Projekt - Komunikacja w sieciach mikrokomputerowych i Techniki multimedialne//
//Sylwia Mazepa, Maciej Sobczyk, Arkadiusz Trębaczewski                       //
//4EF-ZI L3                                                                   //
//01.2020                                                                     //
////////////////////////////////////////////////////////////////////////////////


#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "DHT.h"

#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321

// Polaczenie Wi-Fi
const char* ssid = "smarthouse";
const char* password = "czekoladamilka";

// Adres serwera mqtt
const char* mqtt_server = "192.168.2.1";

// Inicjalizacja klienta esp8266
WiFiClient espClient;
PubSubClient client(espClient);

// Podpiecie ledow do GPIO ESP8266
const int ledGPIO5 = 5;
const int ledGPIO4 = 4;

// DHT
const int DHTPin = 14;

// Inicjalizacja DHT
DHT dht(DHTPin, DHTTYPE);

// Zmienne pomocnicze timerow
long now = millis();
long lastMeasure = 0;

// Podlaczenie 8266 do sieci
void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Laczenie do ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("WiFi polaczone - adres IP: ");
  Serial.println(WiFi.localIP());
}

//komunikat z www
void callback(String topic, byte* message, unsigned int length) {
  Serial.print("Nadeszedl rozkaz do: ");
  Serial.print(topic);
  Serial.print(". Tresc: ");
  String messageTemp;
  
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();



  // Ustawianie stanow GPIO po otrzymaniu wiadomosci przez MQTT
  if(topic=="esp8266/4"){
      Serial.print("Zmiana GPIO 4 na ");
      if(messageTemp == "1"){
        digitalWrite(ledGPIO4, HIGH);
        Serial.print("On");
      }
      else if(messageTemp == "0"){
        digitalWrite(ledGPIO4, LOW);
        Serial.print("Off");
      }
  }
  if(topic=="esp8266/5"){
      Serial.print("Zmiana GPIO 5 na ");
      if(messageTemp == "1"){
        digitalWrite(ledGPIO5, HIGH);
        Serial.print("On");
      }
      else if(messageTemp == "0"){
        digitalWrite(ledGPIO5, LOW);
        Serial.print("Off");
      }
  }
  Serial.println();
}

// Ponawianie polaczenia z serwerem MQTT 
void reconnect() {
  while (!client.connected()) {
    Serial.print("Nawiazywanie polaczenia MQTT...");
    // Proba polaczenia
    if (client.connect("ESP8266Client")) {
      Serial.println("polaczono");  
      client.subscribe("esp8266/4");
      client.subscribe("esp8266/5");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" ponow za 5s");
      delay(5000);
    }
  }
}

// Ustawienie GPIOs jako wyjscia, uruchomienie komunikacji, ustawienie serwera mqtt i callbacka.
void setup() {
  dht.begin();
  pinMode(ledGPIO4, OUTPUT);
  pinMode(ledGPIO5, OUTPUT);
  
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

//sprawdzenie polaczenia
void loop() {
  if (!client.connected()) {
    reconnect();
  }
  if(!client.loop())
    client.connect("ESP8266Client");
    
  now = millis();
  // Wysylanie temperatury i wilgotnosci co 10s
  if (now - lastMeasure > 10000) {
    lastMeasure = now;
    float h = dht.readHumidity();
    // temp w oC
    float t = dht.readTemperature();

    // sprawdzenie czy nie ma bledu odczytu, aby ponowic.
    if (isnan(h) || isnan(t)) {
      Serial.println("Blad odczytu z DHT");
      return;
    }

    // Wyliczenie wartosci temperatury w oC
    float hic = dht.computeHeatIndex(t, h, false);
    static char temperatureTemp[7];
    dtostrf(hic, 6, 2, temperatureTemp);
    
    static char humidityTemp[7];
    dtostrf(h, 6, 2, humidityTemp);

    // Wyslanie wartosci temp i wilgotnosci
    client.publish("/esp8266/temperature", temperatureTemp);
    client.publish("/esp8266/humidity", humidityTemp);
    
    Serial.print("Wilgotnosc: ");
    Serial.print(h);
    Serial.print(" %\t Temperatura: ");
    Serial.print(t);
    Serial.print(" *C ");
    Serial.print("Odczuwalna: ");
    Serial.print(hic);
    Serial.println(" *C ");
  }
}
