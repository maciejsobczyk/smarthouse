#
# Projekt - Komunikacja w sieciach mikrokomputerowych i Techniki multimedialne
# Sylwia Mazepa, Maciej Sobczyk, Arkadiusz Trebaczewski
# 4EF-ZI L3
# 01.2020
#

import paho.mqtt.client as mqtt
from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)


# Callback - CONNACK
def on_connect(client, userdata, flags, rc):
    print("Nawiazano polaczenie. Kod: " + str(rc))

    client.subscribe("/esp8266/temperature")
    client.subscribe("/esp8266/humidity")


# Callback - PUBLISH od ESP8266
def on_message(client, userdata, message):
    print("Otrzymano odpowiedz '" + str(message.payload) + "' w temacie '"
          + message.topic + "' z QoS " + str(message.qos))
    if message.topic == "/esp8266/temperature":
        print("aktualizacja temperatury")
    socketio.emit('dht_temperature', {'data': message.payload})
    if message.topic == "/esp8266/humidity":
        print("aktualizacja wilgotnosci")
    socketio.emit('dht_humidity', {'data': message.payload})


mqttc = mqtt.Client()
mqttc.on_connect = on_connect
mqttc.on_message = on_message
mqttc.connect("localhost", 1883, 60)
mqttc.loop_start()

# Slownik pinow, ich nazw i stanow
pins = {
    4: {'name': 'GPIO 4', 'board': 'esp8266', 'topic': 'esp8266/4', 'state': 'False'},
    5: {'name': 'GPIO 5', 'board': 'esp8266', 'topic': 'esp8266/5', 'state': 'False'}
}

# Wrzucenie slownika do tymczasowych danych
templateData = {
    'pins': pins
}


@app.route("/app")
def main():
    return render_template('main.html', async_mode=socketio.async_mode, **templateData)


@app.route("/")
def index():
    return render_template('index.html')


# Funkcja uruchamiana po URL z nr pinu i akcja na nim
@app.route("/<board>/<changePin>/<action>")
def action(board, changePin, action):
    # Konwersja pina z URL do int
    changePin = int(changePin)
    # Pobranie nazwy urzadzenia dla zmienianego pina
    devicePin = pins[changePin]['name']
    # Jesli akcja w URL = 1
    if action == "1" and board == 'esp8266':
        mqttc.publish(pins[changePin]['topic'], "1")
        pins[changePin]['state'] = 'True'
    if action == "0" and board == 'esp8266':
        mqttc.publish(pins[changePin]['topic'], "0")
        pins[changePin]['state'] = 'False'
    # Dodaj message do tymczasowych danych
    templateData = {
        'pins': pins
    }
    return render_template('main.html', **templateData)


@socketio.on('my event')
def handle_my_custom_event(json):
    print('otrzymany json: ' + str(json))


if __name__ == "__main__":
    socketio.run(app, host='0.0.0.0', port=8181, debug=True)
